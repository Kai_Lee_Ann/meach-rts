﻿using UnityEngine;
using UnityEngine.UI;

namespace KLA.MechaRts
{
    public class GamePlayView : MonoBehaviour
    {
        [SerializeField] private Button alarmButton;
        [SerializeField] private Button allClearButton;
        [SerializeField] private AlarmCommandInput alarm;

        private void OnEnable()
        {
            alarmButton.onClick.AddListener(OnAlarmButtonClicked);
            allClearButton.onClick.AddListener(OnAllClearButtonClicked);

            UpdateButtonVisibility();
        }

        private void OnDisable()
        {
            alarmButton.onClick.RemoveListener(OnAlarmButtonClicked);
            allClearButton.onClick.RemoveListener(OnAllClearButtonClicked);
        }

        private void OnAlarmButtonClicked()
        {
            alarm.ActivateAlarm();
            UpdateButtonVisibility();
        }

        private void OnAllClearButtonClicked()
        {
            alarm.DeactivateAlarm();
            UpdateButtonVisibility();
        }

        private void UpdateButtonVisibility()
        {
            alarmButton.gameObject.SetActive(!alarm.IsAlarmActive);
            allClearButton.gameObject.SetActive(alarm.IsAlarmActive);
        }
    }
}
