﻿using System.Collections.Generic;
using UnityEngine;

namespace KLA.MechaRts
{
    public class CommandQueueView : MonoBehaviour
    {
        [SerializeField] private RobotCommandQueueRepository repository;
        [SerializeField] private Transform container;
        [SerializeField] private Camera mainCamera;
        [SerializeField] private CommandQueueInfo viewPrefab;

        private void Awake()
        {
            foreach (var robot in repository.Robots)
                CreateView(robot);
        }

        private void CreateView(Robot robot)
        {
            Instantiate(viewPrefab, container)
                .SetCamera(mainCamera)
                .SetTarget(robot.transform)
                .SetQueue(repository.Get(robot));
        }
    }
}
