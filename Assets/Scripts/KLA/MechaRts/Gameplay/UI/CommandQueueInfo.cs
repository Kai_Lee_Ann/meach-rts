﻿using TMPro;
using UnityEngine;

namespace KLA.MechaRts
{
    public class CommandQueueInfo : MonoBehaviour
    {
        [SerializeField] private TMP_Text info;
        [SerializeField] private Vector3 offset;

        private Camera _camera;
        private Transform _target;
        private ICommandQueue _queue;

        private void Update()
        {
            UpdatePosition();
            UpdateInfo();
        }

        public CommandQueueInfo SetCamera(Camera camera)
        {
            _camera = camera;
            return this;
        }

        public CommandQueueInfo SetTarget(Transform target)
        {
            _target = target;
            return this;
        }

        public CommandQueueInfo SetQueue(ICommandQueue commandQueue)
        {
            _queue = commandQueue;
            return this;
        }

        private void UpdateInfo()
        {
            var result = "+" + string.Join("\n+ ", _queue.Commands);
            if (info.text != result)
                info.text = result;
        }

        private void UpdatePosition() =>
            transform.position = _camera.WorldToScreenPoint(_target.transform.position) + offset;
    }
}
