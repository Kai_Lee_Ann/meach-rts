﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace KLA.MechaRts
{
    /// <summary>
    /// Не должен подписываться на события извне, а контроллируется напрямую
    /// </summary>
    public class Robot : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent agent;

        public event Action OnItemCollect;
        public event Action OnItemRemove;
        public event Action OnSelect;
        public event Action OnUnselect;

        private Item _item;

        public void Select() => OnSelect?.Invoke();

        public void Unselect() => OnUnselect?.Invoke();

        public void Collect(Item item)
        {
            _item = item;
            // Debug.Log($"Item collected: {item}");

            OnItemCollect?.Invoke();
        }

        public bool TryGetProductionItem(out Item item)
        {
            item = _item;
            return _item != null;
        }

        public void RemoveItem()
        {
            _item = null;
            OnItemRemove?.Invoke();
        }

        public void SetDestination(Vector3 destination) => agent.SetDestination(destination);

        public bool IsDestinationReached()
        {
            if (agent.pathPending)
                return false;

            if (agent.remainingDistance > agent.stoppingDistance)
                return false;

            if (agent.hasPath && agent.velocity.sqrMagnitude != 0f)
                return false;

            return true;
        }
    }
}
