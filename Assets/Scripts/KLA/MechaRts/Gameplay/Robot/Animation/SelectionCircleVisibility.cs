﻿using UnityEngine;

namespace KLA.MechaRts
{
    public class SelectionCircleVisibility : MonoBehaviour
    {
        [SerializeField] private Robot robot;
        [SerializeField] private SpriteRenderer selection;

        private void Awake() => selection.enabled = false;

        private void OnEnable()
        {
            robot.OnSelect += ShowSprite;
            robot.OnUnselect += HideSprite;
        }

        private void OnDisable()
        {
            robot.OnSelect -= ShowSprite;
            robot.OnUnselect -= HideSprite;
        }


        private void ShowSprite() => selection.enabled = true;

        private void HideSprite() => selection.enabled = false;
    }
}
