﻿using UnityEngine;

namespace KLA.MechaRts
{
    public class RobotItemVisibility : MonoBehaviour
    {
        [SerializeField] private Robot robot;
        [SerializeField] private GameObject productionItem;

        private void Awake() => productionItem.SetActive(false);

        private void OnEnable()
        {
            robot.OnItemCollect += OnItemCollected;
            robot.OnItemRemove += OnItemItemRemoved;
        }

        private void OnDisable()
        {
            robot.OnItemCollect -= OnItemCollected;
            robot.OnItemRemove += OnItemItemRemoved;
        }

        private void OnItemCollected() => productionItem.SetActive(true);

        private void OnItemItemRemoved() => productionItem.SetActive(false);
    }
}
