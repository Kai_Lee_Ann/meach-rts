﻿using UnityEngine;
using UnityEngine.AI;

namespace KLA.MechaRts
{
    public class RobotWalkingAnimationSwitcher : MonoBehaviour
    {
        private static readonly int walking = Animator.StringToHash("Walking");

        [SerializeField] private NavMeshAgent agent;
        [SerializeField] private Animator animator;
        [Tooltip("При каком значении квадрата скорости (sqrMagnitude) включать анииацию Idle")]
        [SerializeField] private float turnOnIdleSpeedLimit = 0.5f;

        private void Update()
        {
            bool isWalking = agent.velocity.sqrMagnitude > turnOnIdleSpeedLimit;
            animator.SetBool(walking, isWalking);
        }
    }
}
