﻿using System;
using UnityEngine;

namespace KLA.MechaRts
{
    public class AlarmCommandInput : MonoBehaviour
    {
        [SerializeField] private RobotCommandQueueRepository commandQueueRepository;
        [SerializeField] private PenBuilding penBuilding;

        public bool IsAlarmActive { get; private set; }
        public event Action OnAlarmActivate;

        public void ActivateAlarm()
        {
            IsAlarmActive = true;
            commandQueueRepository.PauseAll();
            commandQueueRepository.CancelAll();
            commandQueueRepository.SwitchToAlertQueues();
            commandQueueRepository.EnqueueForAll(robot => penBuilding.GetExecutionCommand(robot));

            OnAlarmActivate?.Invoke();
        }

        public void DeactivateAlarm()
        {
            IsAlarmActive = false;
            commandQueueRepository.CancelAll();
            commandQueueRepository.SwitchToMainQueues();
            commandQueueRepository.ResumeAll();
        }
    }
}
