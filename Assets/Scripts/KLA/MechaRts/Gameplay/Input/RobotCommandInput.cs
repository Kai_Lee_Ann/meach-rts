﻿using UnityEngine;

namespace KLA.MechaRts
{
    public class RobotCommandInput : MonoBehaviour
    {
        [SerializeField] private GameInput gameInput;
        [SerializeField] private CameraRaycaster raycaster;
        [SerializeField] private RobotSelector robotSelector;
        [SerializeField] private RobotCommandQueueRepository commandQueueRepository;
        [SerializeField] private AlarmCommandInput alarm;

        public void OnEnable() => gameInput.OnRightButtonClick += OnRightButtonClicked;

        private void OnDisable() => gameInput.OnRightButtonClick -= OnRightButtonClicked;

        private void OnRightButtonClicked(Vector3 clickPosition)
        {
            // Если тревога активна, игнорируем основной поток команд
            if (alarm.IsAlarmActive)
                return;

            if (!raycaster.CameraRaycast(clickPosition, out var hit))
                return;

            if (!robotSelector.IsAnyRobotSelected)
                return;

            var robot = robotSelector.SelectedRobot;
            var queue = commandQueueRepository.Get(robot);

            AssignCommands(robot, queue, hit);
        }

        private void AssignCommands(Robot robot, ICommandQueue queue, RaycastHit hit)
        {
            bool isShiftModifier = gameInput.IsShiftModifierActive;
            queue.ExecutionType = isShiftModifier ? ExecutionType.Cycle : ExecutionType.OneShot;

            if (queue.ExecutionType == ExecutionType.OneShot)
                queue.Cancel();

            var movePosition = hit.transform.TryGetComponent<IParkingArea>(out var parkable)
                ? parkable.GetParkingPosition(robot.transform.position)
                : hit.point;

            queue.Enqueue(new MoveCommand(robot, movePosition));

            if (hit.transform.TryGetComponent<Building>(out var building))
                queue.Enqueue(building.GetExecutionCommand(robot));
        }
    }
}
