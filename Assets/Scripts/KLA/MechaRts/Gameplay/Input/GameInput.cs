﻿using System;
using UnityEngine;

namespace KLA.MechaRts
{
    public class GameInput : MonoBehaviour
    {
        public event Action<Vector3> OnLeftButtonClick;
        public event Action<Vector3> OnRightButtonClick;

        public bool IsShiftModifierActive;


        public void Update()
        {
            IsShiftModifierActive = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

            if (Input.GetMouseButtonDown(0))
            {
                OnLeftButtonClick?.Invoke(Input.mousePosition);
                return;
            }

            if (Input.GetMouseButtonDown(1))
            {
                OnRightButtonClick?.Invoke(Input.mousePosition);
                return;
            }
        }
    }
}
