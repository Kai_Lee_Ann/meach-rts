﻿using UnityEngine;

namespace KLA.MechaRts
{
    public class RobotSelector : MonoBehaviour
    {
        [SerializeField] private GameInput gameInput;
        [SerializeField] private AlarmCommandInput alarm;
        [SerializeField] private CameraRaycaster raycaster;

        public Robot SelectedRobot { get; private set; }
        public bool IsAnyRobotSelected => !ReferenceEquals(SelectedRobot, null);


        public void OnEnable()
        {
            gameInput.OnLeftButtonClick += OnLeftButtonClicked;
            alarm.OnAlarmActivate += OnAlarmActivated;
        }

        private void OnDisable()
        {
            gameInput.OnLeftButtonClick -= OnLeftButtonClicked;
            alarm.OnAlarmActivate -= OnAlarmActivated;
        }

        private void OnAlarmActivated() => RemoveSelection();


        private void OnLeftButtonClicked(Vector3 clickPosition)
        {
            // Если тревога активна, то игнорируем основной поток команд
            if (alarm.IsAlarmActive)
                return;

            Robot previousSelection = SelectedRobot;
            // В любом случае сначала убираем выделение
            RemoveSelection();

            if (!raycaster.CameraRaycast(clickPosition, out var hit))
                return;

            if (!hit.transform.TryGetComponent<Robot>(out var robot))
                return;

            // Если кликнули по уже выбранному роботу, не выбираем его снова
            if (previousSelection == robot)
                return;

            Select(robot);
        }

        private void RemoveSelection()
        {
            if (ReferenceEquals(SelectedRobot, null))
                return;

            SelectedRobot.Unselect();
            SelectedRobot = null;

            // Debug.Log($"Remove selection from: {robot.gameObject.name}");
        }

        private void Select(Robot robot)
        {
            SelectedRobot = robot;
            SelectedRobot.Select();

            // Debug.Log($"Selected: {robot.gameObject.name}");
        }
    }
}
