﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KLA.MechaRts
{
    public class RobotCommandQueueRepository : MonoBehaviour
    {
        [field: SerializeField] public Robot[] Robots { get; private set; }

        private IEnumerable<SwappingCommandQueue> Queues => _queuesByRobot.Values.AsEnumerable();

        private readonly Dictionary<Robot, SwappingCommandQueue> _queuesByRobot =
            new Dictionary<Robot, SwappingCommandQueue>();

        public ICommandQueue Get(Robot robot) =>
            _queuesByRobot.TryGetValue(robot, out var queue) ? queue : CreateQueue(robot);

        public void SwitchToMainQueues()
        {
            foreach (var queue in Queues)
                queue.SwitchToMain();
        }

        public void SwitchToAlertQueues()
        {
            foreach (var queue in Queues)
                queue.SwitchToAlert();
        }

        public void ResumeAll()
        {
            foreach (var queue in Queues)
                queue.Resume();
        }

        public void PauseAll()
        {
            foreach (var queue in Queues)
                queue.Pause();
        }

        public void EnqueueForAll(Func<Robot, ICommand> command)
        {
            foreach (var queueByRobot in _queuesByRobot)
            {
                var robot = queueByRobot.Key;
                var queue = queueByRobot.Value;

                queue.Enqueue(command.Invoke(robot));
            }
        }

        public void CancelAll()
        {
            foreach (var queue in Queues)
                queue.Cancel();
        }

        private ICommandQueue CreateQueue(Robot robot)
        {
            var queue = new SwappingCommandQueue();
            _queuesByRobot[robot] = queue;

            return queue;
        }
    }
}
