﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace KLA.MechaRts
{
    public class SwappingCommandQueue : ICommandQueue
    {
        private ICommandQueue _activeQueue;

        private readonly ICommandQueue _main;
        private readonly ICommandQueue _alertQueue;

        public ExecutionType ExecutionType
        {
            get => _activeQueue.ExecutionType;
            set => _activeQueue.ExecutionType = value;
        }

        public IEnumerable<ICommand> Commands => _activeQueue.Commands;

        public SwappingCommandQueue()
        {
            _main = new CommandQueue();
            _alertQueue = new CommandQueue();
            _activeQueue = _main;
        }

        public async UniTask Enqueue(ICommand command) => _activeQueue.Enqueue(command);

        public void Cancel() => _activeQueue.Cancel();

        public void Pause() => _activeQueue.Pause();

        public void Resume() => _activeQueue.Resume();


        public void SwitchToMain() => _activeQueue = _main;
        public void SwitchToAlert() => _activeQueue = _alertQueue;
    }
}
