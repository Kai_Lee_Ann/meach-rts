﻿namespace KLA.MechaRts
{
    public enum ExecutionType
    {
        OneShot,
        Cycle
    }
}
