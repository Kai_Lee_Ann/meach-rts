﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace KLA.MechaRts
{
    public class CommandQueue : ICommandQueue, IDisposable
    {
        private List<ICommand> _commands = new List<ICommand>();
        private List<ICommand> _commandsPauseStash = new List<ICommand>();

        private CancellationTokenSource _cts;
        private int _commandIndex;

        public IEnumerable<ICommand> Commands => _commands;
        public ExecutionType ExecutionType
        {
            get => _executionType;
            set
            {
                _commandIndex = 0;
                _executionType = value;
            }
        }

        private ExecutionType _executionType;


        public async UniTask Enqueue(ICommand command)
        {
            _commands.Add(command);
            if (_cts == null || _cts.IsCancellationRequested)
                await Start();
        }

        public void Cancel()
        {
            if (_cts == null || _cts.IsCancellationRequested)
                return;

            _cts?.Cancel();
            _commands.Clear();
        }

        public void Pause()
        {
            _commandsPauseStash = new List<ICommand>(_commands);
            Cancel();
        }

        public async void Resume()
        {
            _commands = _commandsPauseStash;
            await Start();
        }

        public void Dispose() => _cts?.Dispose();

        private async UniTask Start()
        {
            Cancel();

            _cts = new CancellationTokenSource();

            while (!_cts.IsCancellationRequested && (_commands.Count > 0 || ExecutionType == ExecutionType.Cycle))
            {
                await TryExecuteNextCommand();
                UpdateCommandIndex();
            }

            _cts?.Cancel();
        }

        private async UniTask TryExecuteNextCommand()
        {
            var command = _commands[_commandIndex];
            try
            {
                await command.Execute(_cts.Token);
            }
            catch (OperationCanceledException)
            {
                return;
            }

            if (ExecutionType == ExecutionType.OneShot && _commands.Count > 0)
                _commands.RemoveAt(_commandIndex);
        }

        private void UpdateCommandIndex() =>
            _commandIndex = ExecutionType switch
            {
                ExecutionType.OneShot => 0,
                ExecutionType.Cycle => _commands.Count > 0 ? (_commandIndex + 1) % _commands.Count : 0,
                _ => _commandIndex
            };
    }
}
