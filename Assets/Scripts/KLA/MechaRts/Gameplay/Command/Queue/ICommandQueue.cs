﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace KLA.MechaRts
{
    public interface ICommandQueue
    {
        ExecutionType ExecutionType { get; set; }
        IEnumerable<ICommand> Commands { get; }

        UniTask Enqueue(ICommand command);

        public void Cancel();

        public void Pause();

        public void Resume();
    }
}
