﻿using System.Threading;
using Cysharp.Threading.Tasks;

namespace KLA.MechaRts
{
    public interface ICommand
    {
        UniTask Execute(CancellationToken token);
    }
}
