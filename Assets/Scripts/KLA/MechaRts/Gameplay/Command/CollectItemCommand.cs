﻿using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace KLA.MechaRts
{
    public class CollectItemCommand : ICommand
    {
        private readonly Robot _robot;
        private readonly FactoryBuilding _factoryBuilding;

        public CollectItemCommand(Robot robot, FactoryBuilding factoryBuilding)
        {
            _robot = robot;
            _factoryBuilding = factoryBuilding;
        }

        public async UniTask Execute(CancellationToken token)
        {
            if (_robot.TryGetProductionItem(out _))
            {
                Debug.Log("Robot already carry a production item, limit is reached");
                return;
            }

            Item item = await _factoryBuilding.GiveAwayItem(token);
            _robot.Collect(item);
        }

        public override string ToString() =>
            $"CollectProductionItemCommand(target={_robot.gameObject.name}, " +
            $"factoryBuilding={_factoryBuilding.gameObject.name})";
    }
}
