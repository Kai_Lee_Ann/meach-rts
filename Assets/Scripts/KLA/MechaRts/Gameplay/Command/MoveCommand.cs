﻿using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;


namespace KLA.MechaRts
{
    public class MoveCommand : ICommand
    {
        private readonly Robot _robot;
        private readonly Vector3 _destination;

        public MoveCommand(Robot robot, Vector3 destination)
        {
            _robot = robot;
            _destination = destination;
        }

        public async UniTask Execute(CancellationToken token)
        {
            _robot.SetDestination(_destination);
            await UniTask.WaitUntil(_robot.IsDestinationReached, cancellationToken: token);
        }

        public override string ToString() =>
            $"MoveCommand(target={_robot.gameObject.name}, destination={_destination})";
    }
}
