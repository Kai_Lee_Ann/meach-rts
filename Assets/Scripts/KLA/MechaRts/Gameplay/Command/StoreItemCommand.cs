﻿using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace KLA.MechaRts
{
    public class StoreItemCommand : ICommand
    {
        private readonly Robot _robot;
        private readonly StorageBuilding _storageBuilding;

        public StoreItemCommand(Robot robot, StorageBuilding storageBuilding)
        {
            _robot = robot;
            _storageBuilding = storageBuilding;
        }

        public async UniTask Execute(CancellationToken token)
        {
            if (!_robot.TryGetProductionItem(out var item))
            {
                Debug.Log($"Robot has no item: {_robot.gameObject.name}");
                return;
            }

            await _storageBuilding.StoreItem(item, token);
            _robot.RemoveItem();
        }

        public override string ToString() =>
            $"StoreProductionItemCommand(target={_robot.gameObject.name}, " +
            $"storageBuilding={_storageBuilding.gameObject.name})";
    }
}
