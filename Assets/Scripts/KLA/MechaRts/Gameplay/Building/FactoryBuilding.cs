﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace KLA.MechaRts
{
    public class FactoryBuilding : Building
    {
        [Tooltip("Время создания объекта в миллисекундах")]
        [SerializeField] private int productionTimeMs;

        private bool _isItemReady;

        public event Action<int> OnProductionStart;
        public event Action OnProductionFinish;
        public event Action OnItemCollected;


        private void OnEnable() => ProduceItem();

        public override ICommand GetExecutionCommand(Robot robot) =>
            new CollectItemCommand(robot, this);

        public async UniTask<Item> GiveAwayItem(CancellationToken cts)
        {
            // Ожидаем, пока не завершится процесс производства
            await UniTask.WaitUntil(() => _isItemReady, cancellationToken: cts);

            _isItemReady = false;
            // Запускаем следующий цикл производства
            ProduceItem();

            OnItemCollected?.Invoke();

            return new Item();
        }

        private async UniTask ProduceItem()
        {
            OnProductionStart?.Invoke(productionTimeMs);
            await UniTask.Delay(TimeSpan.FromMilliseconds(productionTimeMs));

            _isItemReady = true;
            OnProductionFinish?.Invoke();
        }
    }
}
