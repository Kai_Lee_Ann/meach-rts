﻿using UnityEngine;

namespace KLA.MechaRts
{
    public class ColliderParkingArea : MonoBehaviour, IParkingArea
    {
        [SerializeField] private Collider parkingZone;

        public virtual Vector3 GetParkingPosition(Vector3 position) => parkingZone.ClosestPoint(position);
    }
}
