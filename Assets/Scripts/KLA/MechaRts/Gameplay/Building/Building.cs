﻿using UnityEngine;

namespace KLA.MechaRts
{
    public abstract class Building : MonoBehaviour
    {
        public abstract ICommand GetExecutionCommand(Robot robot);
    }
}
