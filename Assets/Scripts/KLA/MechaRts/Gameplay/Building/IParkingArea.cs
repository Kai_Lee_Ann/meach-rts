﻿using UnityEngine;

namespace KLA.MechaRts
{
    /// <summary>
    /// Любая сущность, которая имеет место парковки
    /// </summary>
    public interface IParkingArea
    {
        Vector3 GetParkingPosition(Vector3 position);
    }
}
