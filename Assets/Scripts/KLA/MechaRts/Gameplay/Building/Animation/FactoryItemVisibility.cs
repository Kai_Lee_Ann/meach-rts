﻿using UnityEngine;

namespace KLA.MechaRts
{
    public class FactoryItemVisibility : MonoBehaviour
    {
        [SerializeField] public FactoryBuilding FactoryBuilding;
        [SerializeField] public GameObject ProductCube;

        private void Awake() => ProductCube.SetActive(false);

        private void OnEnable()
        {
            FactoryBuilding.OnProductionFinish += OnProductionFinished;
            FactoryBuilding.OnItemCollected += OnItemCollected;
        }

        private void OnDisable()
        {
            FactoryBuilding.OnProductionFinish -= OnProductionFinished;
            FactoryBuilding.OnItemCollected += OnItemCollected;
        }

        private void OnProductionFinished() => ProductCube.SetActive(true);

        private void OnItemCollected() => ProductCube.SetActive(false);
    }
}
