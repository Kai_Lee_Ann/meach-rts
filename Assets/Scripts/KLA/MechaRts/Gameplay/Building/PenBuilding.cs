﻿using UnityEngine;

namespace KLA.MechaRts
{
    public class PenBuilding : Building
    {
        [SerializeField] private Transform movePoint;

        public override ICommand GetExecutionCommand(Robot robot) => new MoveCommand(robot, movePoint.position);
    }
}
