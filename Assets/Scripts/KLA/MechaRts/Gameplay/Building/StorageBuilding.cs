﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace KLA.MechaRts
{
    public class StorageBuilding : Building
    {
        [Tooltip("Время для сбора объекта в миллисекундах")]
        [SerializeField] private int storeTimeMs;

        private bool _isReadyToStore = true;

        public override ICommand GetExecutionCommand(Robot robot) =>
            new StoreItemCommand(robot, this);


        public async UniTask StoreItem(Item item, CancellationToken cts)
        {
            try
            {
                await UniTask.WaitUntil(() => _isReadyToStore, cancellationToken: cts);
            }
            catch (Exception)
            {
                return;
            }

            _isReadyToStore = false;

            StoreItem(item);
        }

        private async UniTask StoreItem(Item item)
        {
            // Debug.Log($"Start item storing for {storeTimeMs} ms");

            await UniTask.Delay(TimeSpan.FromMilliseconds(storeTimeMs));
            _isReadyToStore = true;

            // Debug.Log($"Item stored {item}");
        }
    }
}
