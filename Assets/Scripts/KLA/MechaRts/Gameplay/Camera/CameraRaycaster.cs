﻿using UnityEngine;

namespace KLA.MechaRts
{
    public class CameraRaycaster : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;

        public bool CameraRaycast(Vector3 position, out RaycastHit hit) =>
            Physics.Raycast(mainCamera.ScreenPointToRay(position), out hit);
    }
}
